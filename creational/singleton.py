class Borg:
    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state


class Singleton(Borg):
    def __init__(self, name=None, **kwargs):
        super().__init__()
        self._shared_state.update(**kwargs)
        self.name = name

    def set_name(self, name=None):
        if name:
            self.name = name

    def get_name(self):
        return self.name

    def __str__(self):
        return 'obj: {}, name: {}'.format(str(self._shared_state), self.name)


def main():
    x = Singleton('singleton', HTTP="Hyper Text Transfer Protocol")
    print(x)
    y = Singleton(SNMP="Simple Network Management Protocol")
    print(y)
    y.set_name('not singleton')
    print(x.get_name())


main()
