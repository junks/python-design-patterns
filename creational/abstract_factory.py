from .factory import Dog, Cat, Animal


class AnimalFactory:
    def make_pet(self, name: str):
        raise NotImplementedError()

    def get_food(self):
        raise NotImplementedError()


class DogFactory(AnimalFactory):
    def make_pet(self, name: str):
        return Dog(name)

    def get_food(self):
        return "dog food"


class CatFactory(AnimalFactory):
    def make_pet(self, name: str):
        return Cat(name)

    def get_food(self):
        return "cat food"


class PetFactory:
    def __init__(self, factory: AnimalFactory):
        self._factory = factory

    def make(self, name) -> Animal:
        return self._factory.make_pet(name)

    def get_food(self):
        return self._factory.get_food()


def main():
    pet_factory = PetFactory(DogFactory())

    god = pet_factory.make('Dog name')

    print(god)

    print(pet_factory.get_food())


main()
