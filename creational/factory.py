import enum
"""
To create different type of objects
"""


class AnimalType(enum.Enum):
    Cat = 0
    Dog = 1

    def tostring(self):
        return self.name


class Animal(object):
    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name


class Dog(Animal):
    def speak(self):
        print('Woof')
        return "Woof"


class Cat(Animal):
    def speak(self):
        print('Meowth')
        return "Meowth"


class AnimalObjectFactory:
    _factory = {
        AnimalType.Dog: Dog,
        AnimalType.Cat: Cat
    }

    @classmethod
    def make(cls, *, animal_type: AnimalType, name: str):
        return cls._factory[animal_type](name)


def main():
    dog = AnimalObjectFactory.make(animal_type=AnimalType.Dog, name='dog lol')

    dog.speak()


# main()
