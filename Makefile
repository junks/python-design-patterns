factory:
	python creational/factory.py

abstract-factory:
	python creational/abstract_factory.py

singleton:
	python creational/singleton.py
